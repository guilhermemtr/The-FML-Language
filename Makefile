# see: http://www.ocaml.info/home/ocaml_sources.html#toc16

# put here the names of your source files (in the right order)
SOURCES = Environment.ml syntax.ml parser.mly lexer.mll typechecking/Typeclasses.ml typechecking/Typecheck.ml Semantics.ml Args.ml main.ml

# the name of the resulting executable
RESULT  = main

# generate type information (.annot files)
ANNOTATE = yes

# make target (see manual) : byte-code, debug-code, native-code, ...
all: debug-code

include OCamlMakefile
