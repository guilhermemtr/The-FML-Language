
(* This module is a very complicated memory model *)

type 'a memref = 'a ref

let newcell v = ref v

let set r v = r := v

let get r = !r

let free r = ()
