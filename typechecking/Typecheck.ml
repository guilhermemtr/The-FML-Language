open Syntax
open Environment

type result_type =
	| IntegerType
	| BooleanType
	| NoType
	| UnknownType											of int * result_type ref
	| FunctionType										of result_type * result_type

exception TypeError of result_type * result_type



let rec get_string_of_type t =
	match t with
	| IntegerType -> "int"
	| BooleanType -> "bool"
	| UnknownType (c,_) -> "a"
(*	| FunctionType (t1,t2) -> "("^get_string_of_type t1^"->"^get_string_of_type t2^")"
	| InvalidType -> "?" *)

let show_type ret_type =
	(get_string_of_type ret_type)



let curr_type = ref 0;;
let gen_uk_type () =
	let gen = !curr_type in
		curr_type := !curr_type + 1;
		gen
;;

let create_type () =
	UnknownType (gen_uk_type (), ref NoType)
;;

let typeerror expected received =
	raise (TypeError (expected,received))
;;


let rec typecheck a =
	match a with
	| _ -> IntegerType
