%{
open Syntax


let string_of_position p =
	(string_of_int p.Lexing.pos_lnum) ^":" ^ (string_of_int p.Lexing.pos_bol)

let raiseError () =
	let p1 = (Parsing.rhs_start_pos 1) in
  let p2 = (Parsing.symbol_end_pos()) in
	Parsing.clear_parser ();
  raise (SyntaxError(string_of_position p1, string_of_position p2))

let parse_error s =
  print_endline s;
  flush stdout


%}

%token EOL
%token TRUE FALSE
%token <int> INT
%token <string> ID


%token AND OR NOT
%token AND_BITWISE OR_BITWISE NOT_BITWISE XOR_BITWISE
%token SHIFT_RIGHT_BITWISE SHIFT_LEFT_BITWISE


%token PLUS MINUS MULT DIV REM
%token EQ NEQ GT LT GEQ LEQ

%token ASSIG REF_ASSIG DEREF
%token IF THEN ELSE

%token DOT COMMA QMARK COL SCOL SCOL2
%token LET IN BEGIN END REC
%token ANON_FUN FUN TO

%token OP

%token LPAR RPAR LBRA RBRA

%token PRINTLN PRINT

%token SHOW_ENV

%start main

%type <Syntax.exp> main

%% /* Grammar rules */

main:
	expr SCOL2 { $1 }
;


expr:
	LET scope_expr IN expr { Block([$2],$4) }
|	LET scope_expr { $2 }
| block_expr { $1	}

scope_expr:
	ID ASSIG expr { Declaration($1,$3) }
|	ID ID fun_args { Named_Function($1,$2,$3) }
| block_expr { $1 }

fun_args:
	ASSIG expr { $2 }
|	ID fun_args { Function($1,$2) }
;

block_expr:
  BEGIN block_expr END { Block([],$2) }
|  anon_fun_expr { $1 }
;

anon_fun_expr:
  FUN ID TO anon_fun_expr { Function($2,$4) }
| seq_exp { $1 }
;

/* sequence_expression */
seq_exp:
  seq_exp SCOL ass_exp  { Sequence_Expression($1,$3) }
| ass_exp { $1 }
;


/* assignment_expression */
ass_exp:
  ass_exp REF_ASSIG cond_exp { Reference_Assignment_Expression($1,$3) }
| cond_exp { $1 }
;

/* conditional_expression */
cond_exp:
  IF bool_or THEN bool_or ELSE bool_or { Conditional_Expression($2,$4,$6) }
| bool_or { $1 }
;

/* or */
bool_or:
  bool_or OR bool_and { Boolean_Arithmetic(Or ,$1, $3)  }
| bool_and { $1 }
;

/* and */
bool_and:
  bool_and AND b_or { Boolean_Arithmetic(And ,$1, $3) }
| b_or { $1 }
;

/* bitwise or */
b_or:
  b_or OR_BITWISE b_xor { Bitwise_Arithmetic(Bit_Or ,$1, $3)  }
| b_xor { $1 }
;

/* bitwise xor */
b_xor:
  b_xor XOR_BITWISE b_and { Bitwise_Arithmetic(Bit_Xor ,$1, $3)  }
| b_and { $1 }
;

/* bitwise and */
b_and:
  b_and AND_BITWISE comp { Bitwise_Arithmetic(Bit_And ,$1, $3)  }
| comp { $1 }
;


/* comparison */
comp:
  comp EQ   ineq_comp { Comparison(Eq ,$1, $3)  }
| comp NEQ  ineq_comp { Comparison(Neq,$1, $3) }
| ineq_comp { $1 }
;

/* inequality comparison */
ineq_comp:
  ineq_comp GT  b_s_exp { Comparison(Gt , $1, $3) }
| ineq_comp LT  b_s_exp { Comparison(Lt , $1, $3) }
| ineq_comp GEQ b_s_exp { Comparison(Geq, $1, $3) }
| ineq_comp LEQ b_s_exp { Comparison(Leq, $1, $3) }
| b_s_exp { $1 }
;

/* bit shift left & right */
b_s_exp:
  b_s_exp SHIFT_RIGHT_BITWISE ia_expr { Bitwise_Arithmetic(Bit_Shift_Right,$1,$3) }
| b_s_exp SHIFT_LEFT_BITWISE  ia_expr { Bitwise_Arithmetic(Bit_Shift_Left, $1,$3) }
| ia_expr { $1 }

/* integer arithmetic expression */
ia_expr:
  ia_expr PLUS  ia_fact { Integer_Arithmetic(Add,$1,$3) }
| ia_expr MINUS ia_fact { Integer_Arithmetic(Sub,$1,$3) }
| ia_fact { $1 }
;

/* integer arithmetic factor */
ia_fact:
  ia_fact MULT fact { Integer_Arithmetic(Mul,$1,$3) }
| ia_fact DIV  fact { Integer_Arithmetic(Div,$1,$3) }
| ia_fact REM  fact { Integer_Arithmetic(Mod,$1,$3) }
| fact { $1 }
;

/* Unary operators */
fact:
  DEREF 			fact  { Dereferenciation_Expression($2) 			}
| NOT   			fact  { Unary_Boolean_Arithmetic(Not, $2) 		}
| NOT_BITWISE fact  { Unary_Bitwise_Arithmetic(Bit_Not, $2) }
| MINUS				fact	{ Integer_Arithmetic(Sub, Number 0, $2)	}
| app { $1 }
;

app:
	PRINT term { Print($2) }
|	PRINTLN term { Println($2) }
|	app term   { Function_Application ($1,$2) }
|	term { $1 }
;

term:
  ID                    { Id $1 }
|	expr DOT ID						{	Select($1,$3)	}
| INT                   { Number $1 }
| TRUE                  { Boolean true  }
| FALSE                 { Boolean false }
| LPAR expr RPAR        { $2 }
| LBRA record RBRA			{ Record $2 }
;

record:
	ID ASSIG term { [($1,$3)] }
|	ID ASSIG term SCOL record { ($1,$3)::$5 }

;

%%
