

let verbose 		= ref false
and unparse 		= ref false
and interactive = ref true
and compile			= ref false
;;

let version     =
  "Compiler version 0.0.0"
;;

let speclist = [
	("-u"				  , Arg.Set unparse                              , "Sets unparsing mode  "	);
	("-i"				  , Arg.Set interactive                          , "Sets interactive mode"	);
	("-c"				  , Arg.Set compile                              , "Sets compilation mode"  );
  ("-v"				  , Arg.Set verbose                              , "Enables verbose mode "	);
  ("--version"  , Arg.Unit (fun () -> print_endline version)   , "Displays the version "	);
];;


let rec getUsage l =
  match l with
  | []          -> ""
  | (a,b,c)::xs -> "["^a^"] " ^ getUsage xs
;;

let usage =
  "usage: " ^ Sys.argv.(0) ^ " " ^ getUsage speclist
;;


let readArguments =
  Arg.parse
    speclist
    (fun x -> raise (Arg.Bad ("Bad argument : " ^ x)))
    usage
;;
