module EnvMap = Map.Make(String)

exception EnvironmentError

type 'a env_ty = ('a EnvMap.t) list ref

let beginScope env = env := EnvMap.empty :: !env; ();;

let endScope env = match !env with
    	     	      [] -> raise EnvironmentError
		    | local::env' -> env := env'; ()

let rec find_internal s env =
  match env with
  [] -> raise EnvironmentError
 	| local::env' -> if EnvMap.mem s local then EnvMap.find s local else find_internal s env'

let rec find s env =
  find_internal s (!env)

let assoc s v env = match !env with
    	      	      [] -> raise EnvironmentError
		    | local::env' ->
          let nenv = (EnvMap.add s v local)::env' in
            env := nenv; ()

let show_env_level e f lvl =
  print_endline ("Environment Level:\t" ^ (string_of_int lvl));
  EnvMap.iter (fun k v -> print_endline k;  print_endline (f v); print_endline "") e;
  ()

let rec show_env_internal env f =
  match env with
  | [] -> print_endline "Empty environment"; 0
  | [x] -> show_env_level x f 0; 0
  | x::xs ->
      let env_lvl = (show_env_internal xs f) in
        (show_env_level x f (env_lvl + 1));
        env_lvl + 1

let show_env env f =
  show_env_internal (!env) f; ()

let emptyEnv () = ref []
