open Syntax
open Environment
open Typecheck


type result =
	| Integer 												of int
	| Boolean 												of bool
	| Closure													of string * exp * result env_ty ref
	| RecordValue											of ((string * result) list) ref
	| Unit
	| Undefined

exception DivByZero

let create_env () =
	let envir = emptyEnv () in
		beginScope envir; envir
;;

let env:(result env_ty) =
	create_env ()
;;

let get_env () =
	env
;;

let set_env e =
	env := e
;;

let rec unparse_value v =
	match v with
	| Integer n -> "- : int  = " ^ string_of_int n
	| Boolean b -> "- : bool = " ^ string_of_bool b
	| RecordValue	r -> "- : record = \n{\n" ^ (unparse_rec (!r)) ^ "}"
	| Closure (param,exp,env) -> "- : " ^ param ^ " -> " ^ (show_type (typecheck exp)) ^ " = <fun>"
	| Unit -> "()"
	| Undefined -> "Undefined"

and unparse_rec l =
	match l with
	| [] -> ""
	| (l,v)::xs -> l ^ "\t:\t" ^ (unparse_value v) ^ "\n" ^ (unparse_rec xs)

and show_semantics_env () =
	show_env (get_env ()) unparse_value


and eval a =
	match a with
	| Number n  -> Integer n
	| Boolean b -> Boolean b
	| Function (arg, body) -> Closure(arg,body,ref (ref (!(get_env()))))
	| Named_Function (name, arg, body) ->
					let r_env = ref (get_env ()) in
						assoc name (Closure(arg,body, r_env)) (get_env ());
						r_env := ref (! (get_env ()) );
						Unit

	| Declaration (id,exp) ->
								assoc id (eval exp) (get_env());
								if id = "g" then show_semantics_env () else ();
								eval (Id(id))

	| Id s -> find s (get_env ())

	| Integer_Arithmetic 	(op,l,r) ->
                eval_integer_arithmetic (op,(eval l),(eval r))

	| Boolean_Arithmetic 	(op,l,r) ->
                eval_boolean_arithmetic (op,(eval l),(eval r))

	| Unary_Boolean_Arithmetic (op, exp) ->
                eval_unary_boolean_arithmetic (op, (eval exp))

  | Bitwise_Arithmetic 	(op,l,r) ->
                eval_bitwise_arithmetic (op, (eval l), (eval r))

  | Unary_Bitwise_Arithmetic (op, exp) ->
                eval_unary_bitwise_arithmetic (op, (eval exp))

  | Comparison (op,l,r) ->
                eval_comparison (op, (eval l), (eval r))

  | Conditional_Expression (cond,l,r) ->
                eval_conditional_expression (eval cond, l, r)

	| Sequence_Expression (l,r) ->
                (eval l); (eval r)

	| Block (decls,exp) ->
								eval_block decls exp

	| Reference_Assignment_Expression (l,r) ->
                eval r

	| Dereferenciation_Expression exp ->
                eval exp

	| Function_Application (f, a) ->
								eval_fun_app f a

	| Record (r) ->
								(eval_record r)

	| Select(exp,id) ->
								(eval_select exp id)

	| Print (exp) ->
								let res = (eval exp) in
									print_result print_string res

	| Println (exp) ->
								let res = (eval exp) in
									print_result print_endline res

and print_result print_fn res =
	print_fn (unparse_value res);
	flush stdout; Unit

and integer_arithmetic_operator op =
  match op with
	| Add -> ( + )
	| Sub -> ( - )
	| Mul -> ( * )
	| Div -> ( / )
	| Mod -> ( mod )
and eval_integer_arithmetic exp =
  match exp with
  | (Div, Integer _, Integer 0) -> raise DivByZero
  | (Mod, Integer _, Integer 0) -> raise DivByZero
  | (op , Integer n, Integer m) -> Integer ((integer_arithmetic_operator op) n m)
  | _ -> Undefined


and boolean_arithmetic_operator op =
	match op with
	| And -> ( && )
	| Or 	-> ( || )
and eval_boolean_arithmetic exp =
	match exp with
	| (op, Boolean l, Boolean r) -> Boolean ((boolean_arithmetic_operator op) l r)
  | _ -> Undefined


and unary_boolean_operator op =
	match op with
  | Not -> ( not )
and eval_unary_boolean_arithmetic exp =
	match exp with
  | (op, Boolean b) -> Boolean ( (unary_boolean_operator op) b )
  | _ -> Undefined


and comparison_operator op =
	match op with
	| Gt 	-> ( >  )
	| Lt 	-> ( <  )
	| Geq -> ( >= )
	| Leq	-> ( <= )
	| Eq 	-> ( =  )
	| Neq -> ( <> )
and eval_comparison exp =
	match exp with
  | (op, a, b) -> Boolean ((comparison_operator op) a  b)
  | _ -> Undefined


and bitwise_arithmetic_operator op =
	match op with
	| Bit_And 				-> ( land )
	| Bit_Or 					-> ( lor  )
	| Bit_Xor 				-> ( lxor )
	| Bit_Shift_Left 	-> ( lsl )
	| Bit_Shift_Right -> ( lsr )
and eval_bitwise_arithmetic exp =
	match exp with
	| (op, Integer l, Integer r) -> Integer ((bitwise_arithmetic_operator op) l r)
  | _ -> Undefined


and unary_bitwise_arithmetic_operator op =
	match op with
	| Bit_Not 				-> ( lnot )
and eval_unary_bitwise_arithmetic exp =
	match exp with
	| (op,Integer n) 	-> Integer ( (unary_bitwise_arithmetic_operator op) n )

and eval_conditional_expression exp =
  match exp with
  | (Boolean b, l, r) -> if b then eval l else eval r
  | _ -> Undefined


and assoc_decl_list decls =
	match decls with
	| [] -> ()
	| x::xs ->
		eval x;
		assoc_decl_list xs
and eval_block decls exp =
	beginScope (get_env ());
	assoc_decl_list decls;
	let ret = eval exp in
		endScope (get_env ());
		ret


and eval_fun_app f a =
		let arg_val = eval a in
			let fun_val = eval f in
				let orig_env = !(get_env ()) in
					match fun_val with
					| Closure (arg_id, body, ref_fun_env) ->
						let env = !(!ref_fun_env) in
								set_env env;
								beginScope (get_env ());
								assoc arg_id arg_val (get_env ());
								let ret = eval body in
									endScope (get_env ());
									set_env orig_env;
									ret
				| _ -> Undefined

and eval_record le =
	let r = ref [] in
		let env_val = !(get_env()) in
			beginScope (get_env ());
			assoc "this" (RecordValue r) (get_env());
			let rv =
				mymap
					(
						fun (l,e) ->
							let res = (l, eval e) in
								r := res::(!r);
								res
					)
					le
			in
				set_env env_val;
				r := rv;
				RecordValue r

and eval_select exp id =
	let recV = eval exp in
		match recV with
		| RecordValue l ->
				try
					(List.assoc id (!l))
				with _ -> Undefined
		| _ -> Undefined

and mymap f l =
	match l with
	| [] -> []
	| x::xs -> let v = (f x) in
		v; (*Ensure to eval v first*)
		v::(mymap f xs)
