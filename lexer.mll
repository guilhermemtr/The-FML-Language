{
open Parser
open Lexing

exception UnknownToken of char


let incr_linenum lexbuf =
	let pos = lexbuf.lex_curr_p in
  	lexbuf.lex_curr_p <- {
			pos with
    		pos_lnum = pos.pos_lnum + 1;
        pos_bol = 0;
    }

let incr_colnum lexbuf n =
	let pos = lexbuf.lex_curr_p in
  	lexbuf.lex_curr_p <- {
			pos with
    		pos_lnum = pos.pos_lnum;
        pos_bol = pos.pos_bol + n;
    }

}

let digit = ['0'-'9']
let char = ['a'-'z' 'A'-'Z']

rule token = parse
	| [' ' '\t' '\r'] { incr_colnum lexbuf 1; token lexbuf }
	| ['\n'] { incr_linenum lexbuf; token lexbuf }

	| "println" { incr_colnum lexbuf 7; PRINTLN }
	| "print" 	{ incr_colnum lexbuf 5; PRINT		}


	| '+' { incr_colnum lexbuf 1; PLUS  }
	| '-' { incr_colnum lexbuf 1; MINUS }
	| '*' { incr_colnum lexbuf 1; MULT  }
	| '/' { incr_colnum lexbuf 1; DIV   }
	| '%' { incr_colnum lexbuf 1; REM   }

	| ">>" 	{ incr_colnum lexbuf 2; SHIFT_RIGHT_BITWISE }
	| "<<" 	{ incr_colnum lexbuf 2; SHIFT_LEFT_BITWISE 	}

	| '>' 	{ incr_colnum lexbuf 1; GT 	}
	| '<' 	{ incr_colnum lexbuf 1; LT 	}
	| ">=" 	{ incr_colnum lexbuf 2; GEQ }
	| "<=" 	{ incr_colnum lexbuf 2; LEQ }

	| "==" 	{ incr_colnum lexbuf 2; EQ  }
	| "!=" 	{ incr_colnum lexbuf 2; NEQ }

	| '&' 	{ incr_colnum lexbuf 1; AND_BITWISE }
	| '|' 	{ incr_colnum lexbuf 1; OR_BITWISE  }
	| '~' 	{ incr_colnum lexbuf 1; NOT_BITWISE }
	| '^' 	{ incr_colnum lexbuf 1; XOR_BITWISE }


	| "&&" { incr_colnum lexbuf 2; AND }
	| "||" { incr_colnum lexbuf 2; OR  }
	| "!"  { incr_colnum lexbuf 1; NOT }

	|	'.'  { incr_colnum lexbuf 1; DOT 	 }
	|	','  { incr_colnum lexbuf 1; COMMA }
	| '?'  { incr_colnum lexbuf 1; QMARK }
	|	':'  { incr_colnum lexbuf 1; COL 	 }
	|	';'  { incr_colnum lexbuf 1; SCOL  }
	|	";;" { incr_colnum lexbuf 2; SCOL2 }
	|	'@'  { incr_colnum lexbuf 1; DEREF }

	| '='  { incr_colnum lexbuf 1; ASSIG 		 }
	| ":=" { incr_colnum lexbuf 2; REF_ASSIG }


	| '(' { incr_colnum lexbuf 1; LPAR }
	| ')' { incr_colnum lexbuf 1; RPAR }

	| '{' { incr_colnum lexbuf 1; LBRA }
	| '}' { incr_colnum lexbuf 1; RBRA }

	|	"op" 			{ incr_colnum lexbuf 2; OP 	}

	| "let" 		{ incr_colnum lexbuf 3; LET 	}
	|	"rec"			{ incr_colnum lexbuf 3; REC		}
	| "in" 			{ incr_colnum lexbuf 2; IN  	}
	| "begin" 	{ incr_colnum lexbuf 5; BEGIN }
	| "end" 		{ incr_colnum lexbuf 3; END   }

	| "if" 		{ incr_colnum lexbuf 2; IF	 	}
	| "then"	{ incr_colnum lexbuf 4; THEN 	}
	| "else" 	{ incr_colnum lexbuf 4; ELSE 	}

	| "fun"		{ incr_colnum lexbuf 3; FUN 	}
	| "->"		{ incr_colnum lexbuf 2; TO		}


	| "true"  { incr_colnum lexbuf 4; TRUE  }
	| "false" { incr_colnum lexbuf 5; FALSE }

	| digit+ as num { incr_colnum lexbuf (String.length num); INT(int_of_string num) }

	| char (char | digit)* as word { incr_colnum lexbuf (String.length word); ID word }


	| _ as tk { raise (UnknownToken tk) }
	| eof { raise End_of_file }
