open Parser
open Syntax
open Semantics
open Typecheck
open Environment

let rec prompt lexbuf =
	(
		match !Args.interactive with
		| true 	-> print_string "# "; flush stdout
		| _ 		-> ()
	);
  try
  	let s = Parser.main Lexer.token lexbuf in
			(*show_type (typecheck s (emptyEnv ()));*)
			print_endline (unparse s);
			print_endline (unparse_value (eval s));
			prompt lexbuf
  with
	| SyntaxError (p1,p2) ->
			print_string ("Parsing error:"^p1^" to "^p2^"\n");
			Lexing.flush_input lexbuf; prompt lexbuf;

	| EnvironmentError -> print_endline "EnvironmentError"; ()

	| End_of_file -> ()
(*
	| _ -> let line = string_of_int (lexbuf.lex_curr_p.pos_lnum) in
					let col = string_of_int (lexbuf.lex_curr_p.pos_bol) in
						print_endline ("\nline:\t"^line^"\ncolumn:\t"^col)

*)

let main () =
	Args.readArguments;
  let lexbuf = Lexing.from_channel (stdin) in
			prompt lexbuf
;;

main();;
